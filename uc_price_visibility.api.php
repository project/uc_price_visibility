<?php

/**
 * Alters visibility rules.
 *
 * @param $result
 *   The result array, including node ID and visibility of the product.
 */
function hook_uc_price_visibility_alter($result) {
  $visible = $result['visible'];
  $node = $result['node'];

  if ($visible && $node->nid == 40) {
    $result['visible'] = FALSE;
  }
}
